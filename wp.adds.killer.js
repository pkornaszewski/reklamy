// ==UserScript==
// @name        reklamy.wp.pl
// @namespace   reklamy.wp.pl
// @description Skrypt do usuwania reklam z wp.pl i serwisow powiazanych
// @include     http://www.wp.pl/
// @include     http://*.wp.pl/*
// @include     http://*.money.pl/*
// @version     2
// @grant       none
// ==/UserScript==

var keywords = ['owabgxis.wp.pl', 'adv.wp.pl', 'ads_iframe', 'z34ywazabrz.wp.pl'];
var elements = {'img':['src'], 'a':['href'], 'object': ['data'], 'iframe': ['srcdoc', 'src', 'id']};
var count = 0;
var advFound = 0;
var customElements = ['#newsfeed-player-column'];

function searchForAdds(element, level){
    if(count > 5 && count%3 > 0){
        return;
    }
    if(element.find('div[style]').length >=1){
        element.find('div[style]').each(function( index ) {
        searchForAdds($( this ), (level+1));
        });
    } else{
        $.each(elements, function( elementName, elementAttrs ) {
            $.each(elementAttrs, function( elementAttrKey, elementAttr ) {
                if(checkKeywords(element.find(elementName).attr(elementAttr))){
                    element.parents('div[style]:eq(0)').remove();
                    element.find(elementName).remove();
                }
            });
        });
    }
    if(level == 1){
        console.log('searchForAdds script. Adds removed: ' + advFound);
    }
    if(level == 1 && (count == 2 || count > 5) ){
        searchForAddsElements();
        searchForCustomElements();
    }
}

function searchForAddsElements(){
     $.each(elements, function( elementName, elementAttrs ) {
         $.each(elementAttrs, function( elementAttrKey, elementAttr ) {
             $( elementName ).each(function( index ) {
                 if (checkKeywords($( this ).attr(elementAttr))) {
                     $( this ).remove();
                 }
             });
         });
    });
}

function searchForCustomElements(){
    $.each(customElements, function( index, value ) {
        if($(value).length > 0){
            $(value).remove();
        }
    });
}

function checkKeywords(element){
    var ret = false;
    if(element !== undefined){
        $.each(keywords, function( index, value ) {
            if(element.indexOf(value) !== -1){
                ret=true;
                advFound++;
            }
        });
    }
    return ret;
}

setInterval(function(){ count++; searchForAdds($("body"), 1); }, 1000);
